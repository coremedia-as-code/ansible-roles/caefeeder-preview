# CoreMedia - caefeeder-preview

## Ports

```
40705
40799
40798
40780
40709
```

## dependent services

### database

```
caefeeder_preview_database:
  host: backend_database.int
  port: 3306
  schema: cm_mcaefeeder
  user: cm_mcaefeeder
  password: cm_mcaefeeder
```

### solr

```
caefeeder_preview:
  solr:
    url: http://127.0.0.1:40080/solr
```

### content repository

```
caefeeder_preview:
  repository:
    url: http://content-management-server.int:40180/content-management-server/ior
```
